import hashlib
import json
import logging
import struct

from common.consts import JSON_START_PATTERN

_LOGGER = logging.getLogger(__name__)


def parse_message(message_data):
    result = None

    try:
        if message_data is not None and JSON_START_PATTERN in message_data:
            idx = message_data.index(JSON_START_PATTERN)
            message = message_data[idx:]

            if message is not None:
                result = json.loads(message)

    except Exception as e:
        error_message = (
            f"Failed to read data: {message_data}, "
            f"Error: {e}"
        )

        raise Exception(error_message)

    return result


def get_hashed_password(random, realm, username, password):
    password_str = f"{username}:{realm}:{password}"
    password_bytes = password_str.encode('utf-8')
    password_hash = hashlib.md5(password_bytes).hexdigest().upper()

    random_str = f"{username}:{random}:{password_hash}"
    random_bytes = random_str.encode('utf-8')
    random_hash = hashlib.md5(random_bytes).hexdigest().upper()

    return random_hash


def convert_message(data):
    message_data = json.dumps(data, indent=4)

    header = struct.pack(">L", 0x20000000)
    header += struct.pack(">L", 0x44484950)
    header += struct.pack(">d", 0)
    header += struct.pack("<L", len(message_data))
    header += struct.pack("<L", 0)
    header += struct.pack("<L", len(message_data))
    header += struct.pack("<L", 0)

    message = header + message_data.encode("utf-8")

    return message
