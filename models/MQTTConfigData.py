import os

from common.consts import (
    DEFAULT_MQTT_CLIENT_ID,
    DEFAULT_MQTT_TOPIC_PREFIX,
    TOPIC_COMMAND,
)


class MQTTConfigurationData:
    host: str | None
    username: str | None
    password: str | None
    port: int | None
    topic_prefix: str | None
    topic_command_prefix: str | None

    def __init__(self):
        self.client_id = os.environ.get('MQTT_BROKER_CLIENT_ID', DEFAULT_MQTT_CLIENT_ID)
        self.host = os.environ.get('MQTT_BROKER_HOST')
        self.port = os.environ.get('MQTT_BROKER_PORT', 1883)
        self.username = os.environ.get('MQTT_BROKER_USERNAME')
        self.password = os.environ.get('MQTT_BROKER_PASSWORD')

        self.topic_prefix = os.environ.get('MQTT_BROKER_TOPIC_PREFIX', DEFAULT_MQTT_TOPIC_PREFIX)
        self.topic_command_prefix = f"{self.topic_prefix}{TOPIC_COMMAND}/"
